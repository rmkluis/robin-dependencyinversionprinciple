package org.example;

public interface ContentReader {
     String readFromFile(String filePath);
}
