package org.example;

class UserManager {
    private ReportGenerator reportGenerator;

    UserManager(ReportGenerator reportGenerator) {
        this.reportGenerator = reportGenerator;
    }
    void createUser() {
        // Create user
        System.out.println("User created");
        reportGenerator.generateReport();
    }
}
