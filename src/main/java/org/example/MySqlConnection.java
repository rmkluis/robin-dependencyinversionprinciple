package org.example;

class MySqlConnection implements Connection{
    @Override
    public void connect() {
        // Connect to MySQL database
        System.out.println("Connected to MySQL");
    }
}
