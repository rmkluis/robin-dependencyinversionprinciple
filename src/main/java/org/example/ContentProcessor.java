package org.example;

class ContentProcessor {
    private ContentReader contentReader;

    ContentProcessor(ContentReader contentReader) {
        this.contentReader = contentReader;
    }

    String processContent(String filePath) {
        String content = contentReader.readFromFile(filePath);
        // Process content
        return "Processed content: " + content;
    }
}
