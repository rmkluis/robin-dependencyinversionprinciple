package org.example;

public class GasEngine implements Engine {
    @Override
    public void startEngine() {
        System.out.println("Engine started: Fuel ignited");
    }
    @Override
    public void accelerate() {
        System.out.println("Accelerating with gas engine");
    }
}
