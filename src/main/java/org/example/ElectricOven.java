package org.example;

class ElectricOven implements Appliance {
    @Override
    public void bake() {
        // Baking process
        System.out.println("Baking with electric oven");
    }
}
