package org.example;

class Kitchen {
    private Appliance appliance;
    Kitchen(Appliance appliance) {
        this.appliance = appliance;
    }
    void prepareMeal() {
        appliance.bake();
    }
}
